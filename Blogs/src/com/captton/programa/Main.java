package com.captton.programa;

import java.util.ArrayList;

import com.captton.clases.*;

public class Main {

	public static void main(String[] args) {
		
		ArrayList<Post> listado = new ArrayList(); 
		
		Usuario user1= new Usuario("Jose","jose@mail");
		Usuario user2= new Usuario("Nacho","nacho@hola");
		
		Post p1 = new Post();
		Post p2 = new Post();
		Post p3 = new Post();
		
		
		p1.setTitulo("Titulo de mi primer post");
		
		p2.setTitulo("Titulo de otro post");
		
		p3.setTitulo("Titulo de otro post mas");
		
		user1.postear(p1, "Contenido de mi primer post");
		
		user2.postear(p2, "contenido de mi post");
		
		user2.postear(p3, "contenido de mi otro post");
		
		listado.add(p1);
		
		listado.add(p2);
		
		listado.add(p3);
		
		user1.comentar(p1, "buen post animal");
		
		user2.comentar(p1, "a mi no me gusto tu post");
		
		user2.comentar(p2, "hola soy un comentario");
		
		user2.comentar(p3, "hola soy otro comentario");
			
			for (Post posti: listado) {
				
					System.out.println("Titulo :"+posti.getTitulo());
					System.out.println("Contenido : "+posti.getContenido());
					System.out.println("Usuario :"+posti.getUsuario().getNombre());
					
					posti.ListarComentarios();
					
					System.out.println("===========================================");
				
			}
			
	//	System.out.println(p1.getUsuario().getNombre()+" "+p1.getContenido());
		
		
	}

}
