package com.captton.clases;

import java.util.ArrayList;

public class Usuario {
	
	private String nombre;
	private String email;
	private ArrayList<Post>posts;
	
	// C O N S T R U C T O R E S
	
	public Usuario(String nombre, String email) {
		this.nombre = nombre;
		this.email = email;
		this.posts = new ArrayList<Post>();
	}
	public Usuario() {
	}
	
	// G E T T E R S   Y   S E T T E R S
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	// M � T O D O S
	
	public void postear(Post posteo,String contenido) {
		/*Post posteo = new Post();*/
		posteo.setUsuario(this);
		posteo.setContenido(contenido);
		
		//this.posts.add(posteo);
	}
	
	public void comentar(Post posteo, String contenido) {
		
		
		Comentario coment = new Comentario();
		
		coment.setComentario(contenido);
		
		coment.setUsuario(nombre);
		
		coment.setPost(posteo);
		
		posteo.getComentarios().add(coment);
		
	//	posteo.comentarios.add(coment);
		
	}
}
