package com.captton.clases;

import java.util.ArrayList;

public class Post {
	
	private String titulo;
	private Usuario usuario;
	private String contenido;
	private ArrayList<Comentario> comentarios = new ArrayList<Comentario>();
	
	public Post() {
		
		
	}
	public Post(String titulo, Usuario usuario, String contenido, ArrayList<Comentario> comentarios) {
		super();
		this.titulo = titulo;
		this.usuario = usuario;
		this.contenido = contenido;
		this.comentarios = new ArrayList<Comentario>();
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public String getContenido() {
		return contenido;
	}

	public void setContenido(String contenido) {
		this.contenido = contenido;
	}
	
	public void ListarComentarios() {
		
		System.out.println("listado de comentarios: ");
		
		for (Comentario cor: this.comentarios) {
			
			System.out.println("------------------------------------");
			
			System.out.println("Usuario del comentador: "+cor.getUsuario());
		
			System.out.println("Comentario: "+cor.getComentario());
			
		}
	}

	public ArrayList<Comentario> getComentarios() {
		return comentarios;
	}
	
	


}
